# ========== Elise Config =========

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# set PATH so it includes user's private ~/.local/bin if it exists
#if [ -d "$HOME/.local/bin" ] ; then
#    PATH="$HOME/.local/bin:$PATH"
#fi
#export PATH=$HOME/bin:/usr/local/bin:$PATH
#export PATH=$HOME/.local/bin:$PATH

# set PATH so it includes user's private ~/.local/bin if it exists
#if [ -d "$HOME/.local/bin" ] ; then
#    PATH="$HOME/.local/bin:$PATH"
#fi

# starship cross-shell prompt
#eval "$(starship init bash)"

# Install wlroots and sway from source
## <https://llandy3d.github.io/sway-on-ubuntu/simple_install/>
#export PATH=$HOME/.local/bin:$PATH

# Installing Python 3 packages
## Put the path to the local bin directory into a variable
#py3_local_bin=$(python3 -c 'import site; print(site.USER_BASE + "/bin")')
## Put the directory at the front of the system PATH
#export PATH="$py3_local_bin:$PATH"

# $HOME environment for adding your own Python libraries and executable scripts
## Set your prefered editor to Vim
## `~/bin`: `bin` directory as a place to put executable scripts.
## `~/lib/python`: `lib/python` subdirectory for your Python libraries to your Python path.
## You need to logout and log back in before your local `bin` directory will be in your "search path".
#PYTHONPATH=$HOME/lib/python
#EDITOR=vim

#export PYTHONPATH EDITOR

# qtile
#QTILEPATH=/usr/local/bin/qtile
#export QTILEPATH

# pip
#PIPPATH=$HOME/.local/bin/pip3
#export PIPPATH

# Tmux: getting 256 colors to work in tmux
## <https://unix.stackexchange.com/questions/1045/getting-256-colors-to-work-in-tmux>
#if [ "$COLORTERM" = "gnome-terminal" ]; then
#    export TERM=gnome-256color
#fi

## <https://ostechnix.com/autostart-tmux-session-on-remote-system-when-logging-in-via-ssh/>
#if [ -z "$TMUX" ]; then
#  tmux attach -t default || tmux new -s default
#fi

# MPD
export MPD_HOST="localhost"
export MPD_PORT="6601"


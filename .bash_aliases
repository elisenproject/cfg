# Permanently Set Aliases for ZSH terminal and Bash in Linux
# Shared file named `.bash_aliases`: shares aliases with the files
# `.zshrc` and `.bashrc` config files.
#<https://medium.com/@uneetpatel2149/set-aliases-for-zsh-terminal-and-bash-in-linux-7a500f36ad1c>

#alias python3.8='/home/fish/anaconda3/bin/python3.8'

#alias python3.10='/usr/bin/python3.10'
#alias python='/usr/bin/python3.9'

#alias qtile='/usr/local/bin/qtile'

#alias doom='~/.emacs.d/bin/doom'

#alias config='/usr/bin/git --git-dir=$HOME/dotfiles_bare/ --work-tree=$HOME'

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
